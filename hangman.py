import random

hangman = [
    """
       -----
       |   |
           |
           |
           |
           |
    """,
    """
       -----
       |   |
       O   |
           |
           |
           |
    """,
    """
       -----
       |   |
       O   |
       |   |
           |
           |
    """,
    """
       -----
       |   |
       O   |
      /|   |
           |
           |
    """,
    """
       -----
       |   |
       O   |
      /|\\  |
           |
           |
    """,
    """
       -----
       |   |
       O   |
      /|\\  |
      /    |
           |
    """,
    """
       -----
       |   |
       O   |
      /|\\  |
      / \\  |
           |
    """
]

word_to_guess = ['cat', 'dog', 'elephant', 'lion', 'giraffe', 'tiger',
                 'whale', 'dolphin', 'kangaroo', 'horse']
word = random.choice(word_to_guess)
print('''Hello and welcome to "The Hangman Game"\n
Try to guess a name of an animal''')
print(f'TIP: The name has {len(word)} letters\n')

def hidden_letters(word_to_guess):
    hidden_letters = ["_ " if letter.isalpha() else letter for letter in word_to_guess]
    return "".join(hidden_letters)

print(f"Let's start\n {hidden_letters(word)}\n")

guessed_letters = []
hangman_step = 0
incorrect_guess_count = (-1)

while True:
    letter = input("Type a letter: ").lower()
    
    if letter in guessed_letters:
        print("You've already guessed that letter. Try another one.")
        continue
    
    guessed_letters.append(letter)
    
    if letter in word:
        word_display = ""
        for char in word:
            if char in guessed_letters:
                word_display += char
            else:
                word_display += "_"
        
        print(f"Good guess! Current word: {word_display}")
        
        if "_" not in word_display:
            print(f"Congratulations! You guessed the word: {word}")
            break
    else:
        print("Incorrect guess.")
        incorrect_guess_count += 1
        
        if incorrect_guess_count == 0:
            hangman_step = 0  
        if incorrect_guess_count == 1:
            hangman_step = 1  
        if incorrect_guess_count == 2:
            hangman_step = 2
        if incorrect_guess_count == 3:
            hangman_step = 3
        if incorrect_guess_count == 4:
            hangman_step = 4
        if incorrect_guess_count == 5:
            hangman_step = 5
        if incorrect_guess_count == 6:
            hangman_step = 6
        if incorrect_guess_count == 7:
            hangman_step = 7
            
        print(hangman[hangman_step])
        
        if hangman_step == len(hangman) - 1:
            print("You ran out of attempts. The word was:", word)
            break
